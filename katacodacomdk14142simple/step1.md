
# Basic Commands

Here you execute basic commands.

For example: `ls`{{ execute }}`


# Advanced Commands

Here you execute advanced commands.

For example: `df -h`{{ execute }}

